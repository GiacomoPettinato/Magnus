​Magnus is a turn based, card driven strategy game, 
with added deck building elements. 
Relive the great battles of Alexander the Great 
and conquer Persia!


OVERVIEW

Slitherine as client

5 months of development

14 members (5 designers, 4 programmers, 2 concept artists, 3 3D artists)


WHAT I DID

Main managers (GameManager, CardManager, SessionManager, UIManager, InputManager)

Card behaviours and effects

Camera controller

Deck building

Tools for testing the deck, tuning the troops and implementing assets

UI, troop inspector and behaviours of icons

FSM & animations of troops

Custom save system